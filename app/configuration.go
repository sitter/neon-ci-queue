// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018-2023 Harald Sitter <sitter@kde.org>

package app

import (
	"io/ioutil"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"
)

// Conf is the global configuration object of the app configuration.
var Conf = loadConfiguration()

// Config encapsulates the deserialized app configuration.
type Config struct {
	// sentry dsn
	DSN               string `yaml:"dsn"`
	GitLabAPIToken    string `yaml:"gitlab_api_token"`
	ClientBearerToken string `yaml:"client_bearer_token"`
}

func loadConfiguration() *Config {
	path := filepath.Join(os.Getenv("HOME"), ".config/neon-ci-queue.yaml")
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		panic("missing ~/.config/neon-ci-queue.yaml")
	}

	y := &Config{}
	err = yaml.Unmarshal(yamlFile, y)
	if err != nil {
		panic(err)
	}

	return y
}
