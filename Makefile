# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

all: neon-ci-queue

clean:
	rm -rf neon-ci-queue

test:
	go test -v ./...

neon-ci-queue:
	go install github.com/swaggo/swag/cmd/swag@latest
	go generate
	go build -o neon-ci-queue -v

run: neon-ci-queue
	/usr/bin/systemd-socket-activate -l 0.0.0.0:8080 ./neon-ci-queue

