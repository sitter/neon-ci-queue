// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

package models

import (
	"time"

	"invent.kde.org/sysadmin/neon-ci-queue/invent"
)

type QueueRequest struct {
	Ref         string    `json:"ref" example:"Neon/unstable (CI_COMMIT_BRANCH env var in build)"`
	Source      string    `json:"source" example:"extra-cmake-modules (CI_PROJECT_NAME env var in build)"`
	ProjectID   string    `json:"project_id" example:"the gitlab project ID (CI_PROJECT_ID env var in build)"`
	ProjectPath string    `json:"project_path" example:"the gitlab project path (CI_PROJECT_PATH env var in build)"`
	PipelineID  string    `json:"pipeline_id" example:"the gitlab pipeline ID (CI_PIPELINE_ID env var in build)"`
	PipelineIID string    `json:"pipeline_iid" example:"the project internal pipeline IID (CI_PIPELINE_IID env var in build)"`
	PipelineURL string    `json:"pipeline_url" example:"the gitlab pipeline URL (CI_PIPELINE_URL env var in build)"`
	Time        time.Time `json:"time" example:"the gitlab pipeline creation time in ISO 8601 format (CI_PIPELINE_CREATED_AT env var)"`
}

func (r QueueRequest) ID() string {
	return invent.RefToType(r.Ref) + "!" + r.Source
}
