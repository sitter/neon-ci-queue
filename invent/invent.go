// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2021-2023 Harald Sitter <sitter@kde.org>

package invent

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/Khan/genqlient/graphql"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/xanzy/go-gitlab"
	"invent.kde.org/sysadmin/neon-ci-queue/app"
)

// TODO re-evaluate the need for a REST client

type GitlabFactory interface {
	NewClient() (GitlabClient, error)
}

type GitlabClient interface {
	ListBuildingProjects() []string
	ListPublishingProjects() []string
	PlayJob(projectFullPath string, pipelineIID string, jobName string)
}

type graphQLClientFacade struct {
	// Unfortunately we cannot recycle the gitlab API client because it implicitly closes the body preventing
	// graphql from reading it. Instead we open a secondary client just for the graphql transactions.
	client *retryablehttp.Client
}

func (q graphQLClientFacade) Do(req *http.Request) (*http.Response, error) {
	if values := req.Header.Values("Authorization"); len(values) == 0 {
		req.Header.Set("Authorization", "Bearer "+app.Conf.GitLabAPIToken)
	}
	retryReq, err := retryablehttp.FromRequest(req)
	if err != nil {
		panic(err)
	}
	return q.client.Do(retryReq)
}

type InventFactory struct{}

func (f InventFactory) NewClient() (GitlabClient, error) {
	client, err := gitlab.NewClient(app.Conf.GitLabAPIToken, gitlab.WithBaseURL("https://invent.kde.org/api/v4"))
	return inventClient{
		client:  client,
		gctx:    context.Background(),
		gclient: graphql.NewClient("https://invent.kde.org/api/graphql", graphQLClientFacade{client: retryablehttp.NewClient()}),
	}, err
}

type inventClient struct {
	client *gitlab.Client

	gctx    context.Context
	gclient graphql.Client
}

func (c inventClient) PlayJob(projectFullPath string, pipelineIID string, jobName string) {
	fmt.Println(projectFullPath, pipelineIID, jobName)
	jobResp, err := getJob(c.gctx, c.gclient, projectFullPath, pipelineIID, jobName)
	if err != nil {
		panic(err)
	}
	fmt.Println(jobResp)
	if jobResp.Project.Pipeline.Job.Status != "MANUAL" {
		fmt.Println("Job status not MANUAL, skipping ahead")
		return // presumably already triggered?
	}
	if !jobResp.Project.Pipeline.Job.ManualJob {
		panic("Job not a manual job?????")
	}

	playResp, err := jobPlay(c.gctx, c.gclient, jobResp.Project.Pipeline.Job.Id)
	if err != nil {
		panic(err)
	}
	fmt.Println(playResp)
}

func IsNeonRef(ref string) bool {
	return strings.Index(ref, "Neon/") == 0
}

func RefToType(ref string) string {
	if !IsNeonRef(ref) {
		panic("ref not a Neon ref " + ref)
	}
	return strings.Replace(ref, "Neon/", "", 1)
}

func (c inventClient) ListBuildingProjects() []string {
	buildingPipelines := []string{}
	cursor := ""
	for {
		resp, err := getNeonPipelines(c.gctx, c.gclient, cursor)
		if err != nil {
			panic(err)
		}
		for _, project := range resp.Projects.Nodes {
			for _, pipeline := range project.Pipelines.Nodes {
				if pipeline.Active {
					id := RefToType(pipeline.Ref) + "!" + project.Path
					buildingPipelines = append(buildingPipelines, id)
					break // skip to next
				}
			}
		}
		if !resp.Projects.PageInfo.HasNextPage {
			break
		}
		cursor = resp.Projects.PageInfo.EndCursor
	}
	return buildingPipelines
}

func (c inventClient) ListPublishingProjects() []string {
	projects := []string{}
	cursor := ""
	for {
		resp, err := getNeonPublishers(c.gctx, c.gclient, cursor)
		if err != nil {
			panic(err)
		}
		for _, project := range resp.Projects.Nodes {
			for _, pipeline := range project.Pipelines.Nodes {
				if !pipeline.Active {
					continue
				}
				if pipeline.Job.Active {
					id := RefToType(pipeline.Ref) + "!" + project.Path
					projects = append(projects, id)
				}
			}
		}
		if !resp.Projects.PageInfo.HasNextPage {
			break
		}
		cursor = resp.Projects.PageInfo.EndCursor
	}
	return projects
}
