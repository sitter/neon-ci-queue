<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
-->

# Description

Manages CI queues such that dependencies are built in order.
