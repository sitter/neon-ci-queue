// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018-2023 Harald Sitter <sitter@kde.org>

package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"runtime/debug"
	"slices"
	"strings"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/activation"
	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"invent.kde.org/sysadmin/neon-ci-queue/app"
	docs "invent.kde.org/sysadmin/neon-ci-queue/docs"
	"invent.kde.org/sysadmin/neon-ci-queue/invent"
	"invent.kde.org/sysadmin/neon-ci-queue/models"
)

//go:generate go run github.com/Khan/genqlient invent/genqlient.yaml
//go:generate swag init --codeExampleFiles ./examples

// sentry-go currently has no support for checkins so we  manually send a heartbeat instead
// https://docs.sentry.io/product/crons/getting-started/http/#heartbeat
func sentryHeartbeat() {
	hub := sentry.CurrentHub()
	dsn, err := sentry.NewDsn(hub.Client().Options().Dsn)
	if err != nil {
		log.Println(err)
		return
	}

	reader := strings.NewReader(`{"status": "ok"}`)
	request, err := http.NewRequest(http.MethodPost, "https://errors-eval.kde.org/api/0/organizations/kde/monitors/neon-ci-queue/checkins/", reader)
	if err != nil {
		log.Println(err)
		return
	}
	for headerKey, headerValue := range dsn.RequestHeaders() {
		request.Header.Set(headerKey, headerValue)
	}
	request.Header.Set("Authorization", fmt.Sprintf("DSN %v", dsn.String()))

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Println(err)
		return
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusCreated {
		log.Println("unexpected response status", response.Status)
		return
	}
}

func sentryTransactor() gin.HandlerFunc {
	return func(c *gin.Context) {
		span := sentry.StartSpan(c, c.Request.Method, sentry.TransactionName(c.Request.URL.Path))
		c.Next()
		span.Finish()
	}
}

func version() string {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == "vcs.revision" {
				return setting.Value
			}
		}
	}
	return ""
}

func uri(c *gin.Context) string {
	url := url.URL{}
	url.Scheme = "http"
	url.Host = c.Request.Host
	return url.String()
}

var queueSourceChan = make(chan models.QueueRequest)
var queuePublishChan = make(chan models.QueueRequest)

// queue godoc
// @Summary Queue Build
// @Description Queue a new build
// @Param request body models.QueueRequest true "queue request"
// @Success 200 {string} random status message
// @Success 202 {string} random status message
// @Failure 400 {string} error description
// @Router /v1/queueSource [post]
func queueSource(c *gin.Context) {
	if c.GetHeader("Authorization") != fmt.Sprintf("Bearer %v", app.Conf.ClientBearerToken) {
		c.String(http.StatusUnauthorized, "")
		return
	}

	queueRequest := models.QueueRequest{}
	if err := c.BindJSON(&queueRequest); err != nil {
		fmt.Println(err)
		return // Bind sets error, no need to panic
	}
	if !invent.IsNeonRef(queueRequest.Ref) {
		c.String(http.StatusExpectationFailed, "not a Neon ref:"+queueRequest.Ref)
	}
	queueSourceChan <- queueRequest

	c.JSON(http.StatusAccepted, nil)
}

type processContext struct {
	queueMap         *map[string]models.QueueRequest
	client           invent.GitlabClient
	buildingProjects []string
	request          models.QueueRequest
	dependees        map[string][]string
	dependencies     map[string][]string
}

func processSourceQueue(dependees map[string][]string, dependencies map[string][]string) {
	queueMap := map[string]models.QueueRequest{}

	client, err := invent.InventFactory{}.NewClient()
	if err != nil {
		panic(err)
	}

	lastQueueRequestTime := time.Now()

	for {
		select {
		case queueRequest := <-queueSourceChan:
			fmt.Println("queueRequest... ", queueRequest.ID())
			// NOTE: Replaces previous queued item if there was one!
			queueMap[queueRequest.ID()] = queueRequest
			lastQueueRequestTime = time.Now()
		case <-time.After(16 * time.Second):
			if len(queueMap) == 0 {
				// Nothing queued
				continue
			}
			fmt.Println("timeout...")
			// TODO: also check a quit channel and save state to disk. also raise quit timeout in main
		}

		if time.Now().Sub(lastQueueRequestTime) < 8*time.Second {
			// queueRequests are still coming in, no need to process anything yet.
			continue
		}

		buildingProjects := client.ListBuildingProjects()
		fmt.Println(buildingProjects)

		// FIXME: need to rescue panics

		for _, req := range queueMap {
			processSourceQueueItem(processContext{
				queueMap:         &queueMap,
				client:           client,
				buildingProjects: buildingProjects,
				request:          req,
				dependees:        dependees,
				dependencies:     dependencies,
			})
		}
	}
}

func processSourceQueueItem(context processContext) {
	// Check if this item had been queued for at least N seconds. This gives
	// potential relations to also get queued. If not, this cannot be dequeued.
	// TODO: A sliding window would be more useful. If jobs keep getting queued
	// then we don't really need to evaluate their relationships yet
	if time.Now().Sub(context.request.Time) < 4*time.Second {
		fmt.Println("Not old enough", context.request)
		return
	}

	// Check if any dependency is also queued. If so, this cannot be dequeued.
	dependencies := context.dependencies[context.request.ID()]
	for queuedID := range *context.queueMap {
		if slices.Contains(dependencies, queuedID) {
			fmt.Println("Dependency queued", context.request, queuedID)
			return
		}
	}

	// Or if any dependency is building. If so, this cannot be dequeued.
	dependencyBuilding := slices.ContainsFunc(context.buildingProjects, func(buildingProject string) bool {
		return slices.Contains(dependencies, buildingProject)
	})
	if dependencyBuilding {
		fmt.Println("Dependency building", context.request)
		return
	}

	// Check if any dependees are building. If so, this cannot be dequeued.
	dependees := context.dependees[context.request.ID()]
	dependeeBuilding := slices.ContainsFunc(context.buildingProjects, func(buildingProject string) bool {
		return slices.Contains(dependees, buildingProject)
	})
	if dependeeBuilding {
		fmt.Println("Dependee building", context.request)
		return
	}

	delete(*context.queueMap, context.request.ID())

	fmt.Println("Triggering!", context.request)
	// TODO: maybe retry after a while if it fails?
	context.client.PlayJob(context.request.ProjectPath, context.request.PipelineIID, "source")
}

// queue godoc
// @Summary Queue Publishing
// @Param request body models.QueueRequest true "queue request"
// @Router /v1/queuePublish [post]
func queuePublish(c *gin.Context) {
	if c.GetHeader("Authorization") != fmt.Sprintf("Bearer %v", app.Conf.ClientBearerToken) {
		c.String(http.StatusUnauthorized, "")
		return
	}

	queueRequest := models.QueueRequest{}
	if err := c.BindJSON(&queueRequest); err != nil {
		fmt.Println(err)
		return // Bind sets error, no need to panic
	}
	if !invent.IsNeonRef(queueRequest.Ref) {
		c.String(http.StatusExpectationFailed, "not a Neon ref:"+queueRequest.Ref)
	}
	queuePublishChan <- queueRequest

	c.JSON(http.StatusAccepted, nil)
}

func processPublishQueue(dependees map[string][]string, dependencies map[string][]string) {
	queueMap := map[string]models.QueueRequest{}

	for {
		select {
		case queueRequest := <-queuePublishChan:
			fmt.Println("publish queueRequest... ", queueRequest.ID())
			// NOTE: Replaces previous queued item if there was one!
			queueMap[queueRequest.ID()] = queueRequest
		case <-time.After(64 * time.Second):
			if len(queueMap) == 0 {
				// Nothing queued
				continue
			}
			fmt.Println("publish timeout...")
			// TODO: also check a quit channel and save state to disk. also raise quit timeout in main
		}

		// TODO: If more than 2 jobs are already publishing we'll not publish!
	}
}

// @title Neon CI Queue
// @version 1.0
// @description Manages the build queue such that pipelines run in the order of their dependencies.
//
// @contact.name Source
// @contact.url https://invent.kde.org/sysadmin/neon-ci-queue
//
// @license.name AGPL-3.0-or-later
// @license.url https://spdx.org/licenses/AGPL-3.0-or-later.html
//
// @host ci-queue.neon.kde.org
// @BasePath /
// @query.collection.format multi
func main() {
	flag.Parse()

	if err := sentry.Init(sentry.ClientOptions{
		Dsn:                app.Conf.DSN,
		TracesSampleRate:   0.25,
		AttachStacktrace:   true,
		Release:            version(),
		Debug:              true,
		EnableTracing:      true,
		ProfilesSampleRate: 0.25,
	}); err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}
	defer sentry.Flush(2 * time.Second)

	heartbeat := time.NewTicker(30 * time.Minute)
	go func() {
		for {
			select {
			case <-heartbeat.C:
				sentryHeartbeat()
			}
		}
	}()

	// We'll only check first level relationships. This avoids cyclic dependencies for the most part and is much faster.
	var dependees = map[string][]string{}
	dependees["unstable!kf6-extra-cmake-modules"] = []string{"unstable!kf6-kconfig", "unstable!kf6-ki18n"}
	dependees["unstable!kf6-kconfig"] = []string{"unstable!kf6-ki18n"}
	dependees["unstable!kf6-ki18n"] = []string{}

	var dependencies = map[string][]string{}
	dependencies["unstable!f6-extra-cmake-modules"] = []string{}
	dependencies["unstable!kf6-kconfig"] = []string{"unstable!kf6-extra-cmake-modules"}
	dependencies["unstable!kf6-ki18n"] = []string{"unstable!kf6-extra-cmake-modules", "unstable!kf6-kconfig"}

	go processSourceQueue(dependees, dependencies)
	go processPublishQueue(dependees, dependencies)

	docs.SwaggerInfo.BasePath = "/"

	log.Println("Ready to rumble...")
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(sentrygin.New(sentrygin.Options{Repanic: true}))
	router.Use(sentryTransactor())
	router.SetTrustedProxies([]string{"127.0.0.1"})

	router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusFound, "/swagger/index.html")
	})
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	router.POST("/v1/queueSource", queueSource)
	router.POST("/v1/queuePublish", queuePublish)

	listeners, err := activation.Listeners()
	if err != nil {
		panic(err)
	}

	log.Println("starting servers")
	var servers []*http.Server
	for _, listener := range listeners {
		server := &http.Server{Handler: router}
		go server.Serve(listener)
		servers = append(servers, server)
	}

	if len(servers) == 0 {
		panic("not started by systemd socket activation")
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// Wait for some quit cause.
	// This could be INT, TERM, QUIT or the db update trigger.
	// We'll then do a zero downtime shutdown.
	// This relies on systemd managing the socket and us doing graceful listener
	// shutdown. Once we are no longer listening, the system starts backlogging
	// the socket until we get restarted and listen again.
	// Ideally this results in zero dropped connections.
	<-quit
	log.Println("servers are shutting down")

	for _, srv := range servers {
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()
		srv.SetKeepAlivesEnabled(false)
		if err := srv.Shutdown(ctx); err != nil {
			log.Fatalf("Server Shutdown: %s", err)
		}
	}

	log.Println("Server exiting")
}
